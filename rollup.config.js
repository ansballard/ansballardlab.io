import { terser } from "rollup-plugin-terser";
import gzip from "rollup-plugin-gzip";
import postcss from "rollup-plugin-postcss-modules";
import nodeResolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import replace from "rollup-plugin-replace";
import filesize from "rollup-plugin-filesize";
import visualizer from "rollup-plugin-visualizer";
import chalk from "chalk";

import postcssPresetEnv from "postcss-preset-env";

if(process.env.VISUALIZER && !process.env.FORMAT) {
  console.log(chalk.yellow("VISUALIZER requires a FORMAT to be specified. Ignoring...\n"));
}

const env = {
  NODE_ENV: process.env.NODE_ENV || "production",
  FORMAT: process.env.FORMAT || undefined,
  MAX_SIZE: process.env.MAX_SIZE || 14,
  VISUALIZER: (process.env.VISUALIZER && process.env.FORMAT) || false
};

console.log(`ENV |
-----
${Object.keys(env).filter(key => typeof env[key] !== "undefined").map(key => `${key}: ${env[key]}`).join("\n")}`);

const config = ({
  input,
  output,
  tsconfig
}) => ({
  input,
  output,
  plugins: [
    nodeResolve(),
    commonjs(),
    replace({
      "process.env.NODE_ENV": JSON.stringify(env.NODE_ENV),
      "process.env.API_URL": JSON.stringify(
        env.API_URL || "http://localhost:3000"
      )
    }),
    postcss({
      modules: {
        camelCase: true
      },
      //@ts-ignore https://github.com/egoist/rollup-plugin-postcss#extract-css
      extract: true,
      plugins: [postcssPresetEnv({
        stage: false,
        features: {
          "nesting-rules": true
        }
      })],
      writeDefinitions: true,
      sourceMap: true,
      minimize: env.NODE_ENV === "production" ? {
          preset: 'default',
      } : false
    }),
    typescript({
      tsconfig,
      cacheRoot: "./node_modules/rpt2_cache"
    })
  ].concat(env.NODE_ENV === "production" ? [
    terser(),
    filesize({
      render: (options, bundle, { gzipSize }) => {
        const KB = +gzipSize.slice(0, -3);
        let color = "bgRed";
        if(KB < (env.MAX_SIZE / 2)) {
          color = "bgGreen"
        } else if (KB < env.MAX_SIZE) {
          color = "bgYellow";
        }
        return `GZIP => ${chalk[color](` ${gzipSize} `)}`;
      }
    }),
    gzip()
  ] : [])
  .concat(env.VISUALIZER ? visualizer({
    filename: './node_modules/.stats/stats.html',
    title: "ansballard.github.io",
    open: true
  }) : [])
});

const options = [{
  input: "./src/index.tsx",
  output: {
    file: "public/bundle.mjs",
    format: "es",
    sourcemap: true
  },
  tsconfig: "./tsconfig.json"
}, {
  input: "./src/index.es5.ts",
  output: {
    file: "public/bundle.js",
    format: "iife",
    sourcemap: true
  },
  tsconfig: "./tsconfig.es5.json"
}].filter(({ output }) => !env.FORMAT || output.format === env.FORMAT);

export default options.map(config);
