import { h, render, Component } from "preact";
import Router from "preact-router";

import "./index.css";

class Container extends Component<{}, {
  gubColorBase: string;
  gubColorMap: {
    r: string;
    g: string;
    b: string;
  };
}> {
  constructor(props) {
    super(props);
    this.state = {
      gubColorBase: "g",
      gubColorMap: {
        r: "00",
        g: "ff",
        b: "ff"
      }
    }
    this.updateGubColor = this.updateGubColor.bind(this);
  }
  root = document.documentElement;
  updateGubColor(e) {
    const max = 500;
    const g = (e.clientY + e.clientX) % max;
    const gCurve = g < max / 2 ? g : max - g;
    const hex = gCurve < 16 ? `0${gCurve.toString(16)}` : gCurve.toString(16);
    const fullHex = ["r", "g", "b"].map(c =>
      this.state.gubColorBase === c ? hex : this.state.gubColorMap[c]
    );
    requestAnimationFrame(() => {
      this.root.style.setProperty("--gub-color", `#${fullHex.join("")}`);
    });
  }
  componentDidMount() {
    document.addEventListener("mousemove", this.updateGubColor);
  }
  componentWillUnmount() {
    document.removeEventListener("mousemove", this.updateGubColor);
  }
  render(props, state) {
    return (
      <h1>Hello World!</h1>
    );
  }
}

render(
  <Container />,
  document.getElementById("ansballard-root")
);
